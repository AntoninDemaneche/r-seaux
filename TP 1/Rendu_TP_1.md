# Rendu TP 1 #

## I. Gather Informations ## 

🌞 récupérer une liste des cartes réseau avec leur nom, leur IP et leur adresse MAC :

```
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:67:2e:f7 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 85977sec preferred_lft 85977sec
    inet6 fe80::8dad:52af:ea13:1766/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:a0:1f:7f brd ff:ff:ff:ff:ff:ff
    inet 192.168.202.4/24 brd 192.168.202.255 scope global dynamic noprefixroute enp0s8
       valid_lft 779sec preferred_lft 779sec
    inet6 fe80::d03f:e1d4:7c2d:4204/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```

🌞 déterminer si les cartes réseaux ont récupéré une IP en DHCP ou non

```
[root@localhost lib]# cd /var/lib/NetworkManager
[root@localhost NetworkManager]# ls
internal-0d176d65-d83f-4da7-a7f2-108337c262ef-enp0s3.lease  NetworkManager-intern.conf  secret_key
internal-6aecd646-fccc-3945-9877-2d19731ca4c8-enp0s8.lease  NetworkManager.state        timestamps
```

🌞 afficher la table de routage de la machine et sa table ARP
```

[root@localhost ~]# arp -an
? (10.0.2.2) at 52:54:00:12:35:02 [ether] on enp0s3
? (192.168.202.2) at 08:00:27:d1:56:86 [ether] on enp0s8
? (192.168.202.3) at 0a:00:27:00:00:0c [ether] on enp0s8
```
🌞 récupérer la liste des ports en écoute (listening) sur la machine (TCP et UDP)

```
[root@localhost ~]# netstat -l
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State
tcp        0      0 0.0.0.0:ssh             0.0.0.0:*               LISTEN
tcp6       0      0 [::]:ssh                [::]:*                  LISTEN
udp        0      0 localhost.locald:bootpc 0.0.0.0:*
udp        0      0 localhost.locald:bootpc 0.0.0.0:*
raw6       0      0 [::]:ipv6-icmp          [::]:*                  7
raw6       0      0 [::]:ipv6-icmp          [::]:*                  7
Active UNIX domain sockets (only servers)
Proto RefCnt Flags       Type       State         I-Node   Path
unix  2      [ ACC ]     SEQPACKET  LISTENING     17666    /run/udev/control
unix  2      [ ACC ]     STREAM     LISTENING     21505    /var/lib/sss/pipes/private/sbus-monitor
unix  2      [ ACC ]     STREAM     LISTENING     21857    /var/lib/sss/pipes/private/sbus-dp_implicit_files.743
unix  2      [ ACC ]     STREAM     LISTENING     20858    /run/dbus/system_bus_socket
unix  2      [ ACC ]     STREAM     LISTENING     20861    /var/run/.heim_org.h5l.kcm-socket
unix  2      [ ACC ]     STREAM     LISTENING     17573    /run/systemd/private
unix  2      [ ACC ]     STREAM     LISTENING     21883    /var/lib/sss/pipes/nss
unix  2      [ ACC ]     STREAM     LISTENING     28642    /run/user/0/systemd/private
unix  2      [ ACC ]     STREAM     LISTENING     28651    /run/user/0/bus
unix  2      [ ACC ]     STREAM     LISTENING     10994    /run/systemd/journal/stdout
unix  2      [ ACC ]     SEQPACKET  LISTENING     17658    /run/systemd/coredump
unix  2      [ ACC ]     STREAM     LISTENING     17660    /run/lvm/lvmpolld.socket
Active Bluetooth connections (only servers)
Proto  Destination       Source            State         PSM DCID   SCID      IMTU    OMTU Security
Proto  Destination       Source            State     Channel


```

## II. Edit configuration ##


Les nouvelles ip sont les suivante 10.0.0.1 et 10.0.0.3

```
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:67:2e:f7 brd ff:ff:ff:ff:ff:ff
    inet 10.0.0.1/24 brd 10.0.0.255 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::8dad:52af:ea13:1766/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:a0:1f:7f brd ff:ff:ff:ff:ff:ff
    inet 192.168.202.4/24 brd 192.168.202.255 scope global dynamic noprefixroute enp0s8
       valid_lft 1064sec preferred_lft 1064sec
    inet6 fe80::d03f:e1d4:7c2d:4204/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:18:82:39 brd ff:ff:ff:ff:ff:ff
    inet 10.0.0.3/8 brd 10.255.255.255 scope global noprefixroute enp0s9
       valid_lft forever preferred_lft forever
    inet 192.168.56.4/24 brd 192.168.56.255 scope global dynamic noprefixroute enp0s9
       valid_lft 1064sec preferred_lft 1064sec
    inet6 fe80::a00:27ff:fe18:8239/64 scope link
       valid_lft forever preferred_lft forever

```

Pour le NIX teaming : 

```
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,SLAVE,UP,LOWER_UP> mtu 1500 qdisc fq_codel master bond0 state UP group default qlen 1000    link/ether 08:00:27:67:2e:f7 brd ff:ff:ff:ff:ff:ff
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:a0:1f:7f brd ff:ff:ff:ff:ff:ff
    inet 192.168.202.4/24 brd 192.168.202.255 scope global dynamic noprefixroute enp0s8
       valid_lft 1050sec preferred_lft 1050sec
    inet6 fe80::d03f:e1d4:7c2d:4204/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:18:82:39 brd ff:ff:ff:ff:ff:ff
    inet 10.0.0.3/8 brd 10.255.255.255 scope global noprefixroute enp0s9
       valid_lft forever preferred_lft forever
    inet 192.168.56.4/24 brd 192.168.56.255 scope global dynamic noprefixroute enp0s9
       valid_lft 1048sec preferred_lft 1048sec
    inet6 fe80::a00:27ff:fe18:8239/64 scope link
       valid_lft forever preferred_lft forever
5: bond0: <BROADCAST,MULTICAST,MASTER,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether 08:00:27:67:2e:f7 brd ff:ff:ff:ff:ff:ff
    inet 192.168.246.130/24 brd 192.168.246.255 scope global noprefixroute bond0
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe67:2ef7/64 scope link tentative
       valid_lft forever preferred_lft forever
[root@localhost network-scripts]# arp -an
? (192.168.202.2) at 08:00:27:d1:56:86 [ether] on enp0s8
? (192.168.202.3) at 0a:00:27:00:00:0c [ether] on enp0s8
? (192.168.56.2) at 08:00:27:eb:7e:2b [ether] on enp0s9

```

La carte Bond0 agrége les carte Enp0s3 et Enp0s9.

Pour le Serveur SSH


Je change alors le port shh de ma VM en allant /etc/ssh/sshd_config et en changeant le port 22 part 2222.
Une fois chose faite le restart les configurations shh.
Et j'add le port 2222 en faisant : 
```
sudo firewall-cmd --zone=public --add-port=2222/tcp --permanent
```

( Bon est la ca marche pas enfaite la connections shh n'est pas possible :x )


## III. Routage simple : ## 

Tableau récapitulatif des IPS : 
```
VM1 : 10.0.0.1 
Router : 10.0.0.1 / 10.0.0.2
VM2 : 10.0.0.2
```
On configure les VM pour quels quels communique avec le router, celui-ci doit êtres connecter à internet.