 # TP2 : Network low-level, Switching #

## I. Simplest setup ##

### 🌞 mettre en place la topologie ci-dessus : 

![](image/topo1.png)

### 🌞 faire communiquer les deux PCs :

```
PC1> ping 10.2.1.2
84 bytes from 10.2.1.2 icmp_seq=1 ttl=64 time=0.155 ms
84 bytes from 10.2.1.2 icmp_seq=2 ttl=64 time=0.252 ms
84 bytes from 10.2.1.2 icmp_seq=3 ttl=64 time=0.254 ms
84 bytes from 10.2.1.2 icmp_seq=4 ttl=64 time=0.245 ms
84 bytes from 10.2.1.2 icmp_seq=5 ttl=64 time=0.252 ms
```

```
PC2> ping 10.2.1.1
84 bytes from 10.2.1.1 icmp_seq=1 ttl=64 time=0.250 ms
84 bytes from 10.2.1.1 icmp_seq=2 ttl=64 time=0.257 ms
84 bytes from 10.2.1.1 icmp_seq=3 ttl=64 time=0.266 ms
84 bytes from 10.2.1.1 icmp_seq=4 ttl=64 time=0.291 ms
84 bytes from 10.2.1.1 icmp_seq=5 ttl=64 time=0.267 ms

```

Grâce à Wire Shark on observer les protocoles et on apprend que ce sont des protocole IMCP.

Pour les tables ARP /

```
00:50:79:66:68:01  10.2.1.2 expires in 103 seconds
```
```

00:50:79:66:68:00  10.2.1.1 expires in 114 seconds

```

### 🌞 récapituler toutes les étapes (dans le compte-rendu, à l'écrit) quand PC1 exécute ping PC2 pour la première fois : 

Lors du lancement de la commande 
```
ping 192.168.56.20
```
sur pc1, pc1 va envoyer une requéte arp à pc2 afin de connaitre son adresse MAC pour ensuite envoyer le ping en protocol ICMP.  
Une fois que le ping est arrivé a pc2, pc2 enregistre a q'elle MAC correspond cette ip et il envoie une réponse au ping.


### 🌞 Expliquer...

```
Le switch n'a pas besoin d'ip car ne fait que transmettre les demandes arp aux pc reliés au switch.  
Une ip est nécessaire pour un ping car il permet d'envoyer le message arp pour connaitre l'adresse mac du destinataire afin d'envoyer le ping.  

```


## 2-More switches

### 🌞 Mettre en place la topologie

![](image/topo2.png)

### 🌞 Faire communiquer les 3 pc

On voit que les 3 vm peuvent communiquer entre elle ? 

![](image/commuvm.png)

### 🌞 Analyser la table MAC d'un switch

Voici la table MAC du switch IOU1 :

![](image/table.mac.png)

La premiére ligne affiche l'adresse MAC du switch IOU2 relié par le port Et0/1 du switch, la deuxieme correspond a pc1 et la troisieme au switch IOU3.

### 🐙 Trames CDP
La trame CDP (Cisco Discovery Protocol) Permet de connaitre les informations importantes concernant les machines présentes sur le réseau.  


### III. Isolation ###

### 🌞 mettre en place la topologie ci-dessus avec des VLANs

 ![](image/VLAN.png)

 ### 🌞 faire communiquer les PCs deux à deux

Le ping du pc 1 au pc 2 et 3 :

![](image/ping1213.png)

Ping pc2 pc3, pc3 pc2 

![](image/ping23.png)
![](image/ping32.png)


### 🌞 mettre en place la topologie ci-dessus 

![](image/trunck.png)

### 🌞 faire communiquer les PCs deux à deux

vérifier que PC1 ne peut joindre que PC3

```
PC1> ping 10.2.10.2
84 bytes from 10.2.10.2 icmp_seq=1 ttl=64 time=1.261 ms
84 bytes from 10.2.10.2 icmp_seq=2 ttl=64 time=1.025 ms
84 bytes from 10.2.10.2 icmp_seq=3 ttl=64 time=1.255 ms
84 bytes from 10.2.10.2 icmp_seq=4 ttl=64 time=1.098 ms
84 bytes from 10.2.10.2 icmp_seq=5 ttl=64 time=0.973 ms

```

```
PC1> ping 10.2.20.1 
host (10.2.10.1) not reachable
```

```
PC1> ping 10.2.20.2
host (10.2.20.2) not reachable
```

vérifier que PC4 ne peut joindre que PC2

```
PC4> ping 10.2.20.1
84 bytes from 10.2.20.1 icmp_seq=1 ttl=64 time=0.803 ms
84 bytes from 10.2.20.1 icmp_seq=2 ttl=64 time=1.107 ms
84 bytes from 10.2.20.1 icmp_seq=3 ttl=64 time=1.240 ms
84 bytes from 10.2.20.1 icmp_seq=4 ttl=64 time=1.231 ms
84 bytes from 10.2.20.1 icmp_seq=5 ttl=64 time=1.015 ms

```

```
PC4> ping 10.2.10.1 
host (10.2.10.1) not reachable
```

```
PC4> ping 10.2.10.2
host (10.2.10.2) not reachable
```
